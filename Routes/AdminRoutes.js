'use strict';
/**
 * Created by prince on 10/7/15.
 */

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');

var non_auth_routes = [
];

var userRoutes = [
];

var adminLogin = [

    {
        method: 'POST',
        path: '/api/admin/login',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.adminLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin login',
            tags: ['api', 'admin','login'],
            validate: {
                payload: {
                    email:Joi.string().trim().required(),
                    password:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/admin/listUsers',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.AdminController.listUsers(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin list Users',
            tags: ['api', 'admin','list Users'],
            validate: {
                query: {
                    accessToken:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/admin/violationReports',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.AdminController.violationReports(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin violation reports',
            tags: ['api', 'admin','violation reports'],
            validate: {
                query: {
                    accessToken:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/admin/violationPoints',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.AdminController.violationPoints(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin violation points',
            tags: ['api', 'admin','get violation points'],
            validate: {
                query: {
                    accessToken:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/admin/addViolationPoint',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.addViolationPoint(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin add violation points',
            tags: ['api', 'admin','add violation points'],
            validate: {
                payload: {
                    accessToken:Joi.string().trim().required(),
                    violationPoint:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/admin/deleteViolationPoint',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.deleteViolationPoint(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin delete violation points',
            tags: ['api', 'admin','delete violation points'],
            validate: {
                payload: {
                    accessToken:Joi.string().trim().required(),
                    violationPointId:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/api/admin/editViolationPoint',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.editViolationPoint(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin edit violation point',
            tags: ['api', 'admin','edit violation points'],
            validate: {
                payload: {
                    accessToken:Joi.string().trim().required(),
                    violationPointId:Joi.string().trim().required(),
                    violationPoint:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/admin/getBPNIPosts',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.AdminController.getBPNIPosts(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin get BPNI post',
            tags: ['api', 'admin','get BPNI posts'],
            validate: {
                query: {
                    accessToken:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/admin/addOrEditBPNIPost',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.addBPNIPosts(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin add/edit bpni post',
            tags: ['api', 'admin','add edit bpni post'],
            validate: {
                payload: {
                    accessToken:Joi.string().trim().required(),
                    title:Joi.string().required(),
                    postBody:Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/admin/getCounsellors',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.AdminController.getCounsellors(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin get Counsellors',
            tags: ['api', 'admin','get Counsellors'],
            validate: {
                query: {
                    accessToken:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/admin/addCounsellor',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.addCounsellor(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin add counsellor',
            tags: ['api', 'admin','add counsellor'],
            payload: {
                maxBytes: 10485760,
                parse: true,
                output: 'file'
            },
            validate: {

                payload: {
                    accessToken:Joi.string().trim().required(),
                    name:Joi.string().trim().required(),
                    email:Joi.string().trim().required(),
                    contactNo:Joi.string().trim().required(),
                    latitude:Joi.string().trim().required(),
                    longitude:Joi.string().trim().required(),
                    address:Joi.string().trim().required(),
                    pic: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file')
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/api/admin/editCounsellor',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.editCounsellor(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin edit counsellor',
            tags: ['api', 'admin','edit counsellor'],
            payload: {
                maxBytes: 10485760,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    accessToken:Joi.string().trim().required(),
                    counsellorId:Joi.string().trim().required(),
                    email:Joi.string().trim().required(),
                    name:Joi.string().trim().required(),
                    contactNo:Joi.string().trim().required(),
                    latitude:Joi.string().trim().required(),
                    longitude:Joi.string().trim().required(),
                    address:Joi.string().trim().required(),
                    imageUrl:Joi.string().trim().optional(),
                    pic: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file')

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/admin/deleteCounsellor',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.deleteCounsellor(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin delete counsellor',
            tags: ['api', 'admin','delete counsellor'],
            validate: {
                payload: {
                    accessToken:Joi.string().trim().required(),
                    counsellorId:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/admin/listDonations',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.AdminController.listDonations(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin list Donations',
            tags: ['api', 'admin','list donations'],
            validate: {
                query: {
                    accessToken:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/admin/listPetitions',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.AdminController.listPetitions(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin list Petitions',
            tags: ['api', 'admin','list petitions'],
            validate: {
                query: {
                    accessToken:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/admin/addPetition',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.addPetition(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin add Petitions',
            tags: ['api', 'admin','add petitions'],
            payload: {
                maxBytes: 10485760,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    accessToken:Joi.string().trim().required(),
                    name:Joi.string().trim().required(),
                    description:Joi.string().trim().required(),
                    pic: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file')

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/api/admin/editPetition',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.editPetition(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin edit petition',
            tags: ['api', 'admin','edit petition'],
            payload: {
                maxBytes: 10485760,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    accessToken:Joi.string().trim().required(),
                    petitionId:Joi.string().trim().required(),
                    name:Joi.string().trim().required(),
                    description:Joi.string().trim().required(),
                    imageUrl:Joi.string().trim().optional(),
                    pic: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file')
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/admin/deletePetition',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.deletePetition(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin delete petition',
            tags: ['api', 'admin','delete petition'],
            validate: {
                payload: {
                    accessToken:Joi.string().trim().required(),
                    petitionId:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/admin/listSignedPetitions',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.AdminController.listSignedPetitions(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin list Signed Petitions',
            tags: ['api', 'admin','list signed petitions'],
            validate: {
                query: {
                    accessToken:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/admin/listCounsellorRequests',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.AdminController.listCounsellorRequests(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin list Counsellor Requests',
            tags: ['api', 'admin','list counsellor requests'],
            validate: {
                query: {
                    accessToken:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/admin/listNewEvents',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.AdminController.listNewEvents(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin list New Events (whats new)',
            tags: ['api', 'admin','list new events'],
            validate: {
                query: {
                    accessToken:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/admin/addNewEvent',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.addNewEvent(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin add New Event',
            tags: ['api', 'admin','add new event'],
            validate: {
                payload: {
                    accessToken:Joi.string().trim().required(),
                    text:Joi.string().trim().required(),
                    title:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/api/admin/editNewEvent',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.editNewEvent(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin edit event',
            tags: ['api', 'admin','edit event'],
            validate: {
                payload: {
                    accessToken:Joi.string().trim().required(),
                    eventId:Joi.string().trim().required(),
                    text:Joi.string().trim().required(),
                    title:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/admin/deleteNewEvent',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.deleteNewEvent(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin delete event',
            tags: ['api', 'admin','delete event'],
            validate: {
                payload: {
                    accessToken:Joi.string().trim().required(),
                    eventId:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/api/admin/editAboutBPNIandT&C',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.editAboutBPNI(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin edit About BPNI and t&c',
            tags: ['api', 'admin','edit About BPNI and t&c'],
            validate: {
                payload: {
                    accessToken:Joi.string().trim().required(),
                    aboutBPNI:Joi.string().trim().required(),
                    tAndC:Joi.string().trim().required(),
                    donationText:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/admin/listAboutBPNI',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.AdminController.listAboutBPNI(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin list about BPNI AND terms and conditions',
            tags: ['api', 'admin','list about bpni'],
            validate: {
                query: {
                    accessToken:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/api/admin/changePassword',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.AdminController.changePassword(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            });
        },
        config: {
            description: 'Admin change password',
            tags: ['api', 'admin','change password'],
            validate: {
                payload: {
                    accessToken:Joi.string().trim().required(),
                    oldPassword:Joi.string().trim().required(),
                    newPassword:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    }


];


var authRoutes = [].concat(userRoutes, adminLogin);

module.exports = authRoutes.concat(non_auth_routes);