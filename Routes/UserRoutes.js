var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');


module.exports = [

    {
        method: 'POST',
        path: '/api/user/signupWithEmail',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.signupWithEmail(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'Register user by email',
            tags: ['api', 'user', 'signup', 'email'],
            validate: {
                payload: {
                    name: Joi.string().required(),
                    gender: Joi.string().optional(),
                    age: Joi.string().optional(),
                    latitude: Joi.string().required(),
                    longitude: Joi.string().required(),
                    address: Joi.string().required(),
                    email: Joi.string().required(),
                    password: Joi.string().required(),
                    deviceToken: Joi.string().required(),
                    deviceType: Joi.string().required().valid(
                        [UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
                            UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID])
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/user/signupWithFb',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.createFBUser(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Register user by fb',
            tags: ['api', 'user', 'signup', 'Fb'],
            validate: {
                payload: {
                    name: Joi.string().optional(),
                    gender: Joi.string().optional(),
                    age: Joi.string().optional(),
                    email: Joi.string().required(),
                    fbId: Joi.string().required(),
                    latitude:Joi.string().optional(),
                    longitude:Joi.string().optional(),
                    address:Joi.string().optional(),
                    deviceToken: Joi.string().required(),
                    deviceType: Joi.string().required().valid(
                        [UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
                            UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID])
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/user/fbLogin',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.fbLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'fb login',
            tags: ['api', 'user', 'login', 'Fb'],
            validate: {
                payload: {
                    fbId: Joi.string().required(),
                    deviceToken: Joi.string().required(),
                    deviceType: Joi.string().required().valid(
                        [UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
                            UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID])
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/user/emailLogin',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.emailLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'email login',
            tags: ['api', 'user', 'login', 'email'],
            validate: {
                payload: {
                    email: Joi.string().required(),
                    password: Joi.string().required(),
                    deviceToken: Joi.string().required(),
                    deviceType: Joi.string().required().valid(
                        [UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
                            UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID])
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/user/accessTokenLogin',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.accessTokenLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'access token login',
            tags: ['api', 'user', 'login', 'access token'],
            validate: {
                payload: {
                    accessToken: Joi.string().required(),
                    deviceToken: Joi.string().required(),
                    deviceType: Joi.string().required().valid(
                        [UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
                            UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID])
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/user/logout',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.logout(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'logout',
            tags: ['api', 'user', 'logout'],
            validate: {
                payload: {
                    accessToken: Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/user/reportViolation',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.reportViolation(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'report violation',
            tags: ['api', 'user', 'report violation'],
            payload: {
                maxBytes: 10485760,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    accessToken: Joi.string().required(),
                    title: Joi.string().required(),
                    subject: Joi.string().required(),
                    publishedDate: Joi.string().required(),
                    textBox: Joi.string().required(),
                    pic: Joi.any()
                        .meta({swaggerType: 'file'})
                        .required()
                        .description('image file'),
                    pic1: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file'),
                    pic2: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file'),
                    pic3: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file')
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/user/listViolationPoints',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.listViolationPoints(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'list violation points',
            tags: ['api', 'user', 'list violation points'],
            validate: {
                query: {
                    accessToken: Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'form',
                        responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                    }
                }

        }
    },

    {
        method: 'GET',
        path: '/api/user/listBPNIPosts',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.listBPNIPosts(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'list BPNI posts',
            tags: ['api', 'user', 'list BPNI posts'],
            validate: {
                query: {
                    accessToken: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'form',
                        responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                    }
                }

        }
    },

    {
        method: 'GET',
        path: '/api/user/listCounsellors',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.listCounsellors(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'list Counsellors',
            tags: ['api', 'user', 'list Counsellors'],
            validate: {
                query: {
                    accessToken: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }

        }
    },

    {
        method: 'GET',
        path: '/api/user/listPetitions',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.listPetitions(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'list Petitions',
            tags: ['api', 'user', 'list petitions'],
            validate: {
                query: {
                    accessToken: Joi.string().trim().required(),
                   notificationScreen:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }

        }
    },

    {
        method: 'POST',
        path: '/api/user/signupPetition',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.signupPetition(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Signup Petition',
            tags: ['api', 'user', 'signup petition'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    petitionId:Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }

        }
    },

    {
        method: 'GET',
        path: '/api/user/getUserDetails',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.getUserDetails(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'get user details',
            tags: ['api', 'user', 'get user details'],
            validate: {
                query: {
                    accessToken: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }

        }
    },

    {
        method: 'POST',
        path: '/api/user/counsellorRequest',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.CounsellorRequests(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Counsellor Request',
            tags: ['api', 'user', 'counsellor request'],
            payload: {
                maxBytes: 10485760,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    name:Joi.string().trim().required(),
                    age:Joi.string().trim().required(),
                    gender:Joi.string().trim().required(),
                    description:Joi.string().trim().required(),
                    email:Joi.string().trim().required(),
                    contactNo:Joi.string().trim().required(),
                    pic: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file')

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }

        }
    },

    {
        method: 'PUT',
        path: '/api/user/editProfile',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.editProfile(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Edit Profile',
            tags: ['api', 'user', 'edit profile'],
            payload: {
                maxBytes: 10485760,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    name:Joi.string().trim().required(),
                    age:Joi.string().trim().required(),
                    gender:Joi.string().trim().required(),
                    latitude:Joi.string().trim().required(),
                    longitude:Joi.string().trim().required(),
                    address:Joi.string().trim().required(),
                    pic: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file'),
                    imageUrl:Joi.string().trim().optional(),
                    email:Joi.string().trim().required()

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }

        }
    },

    {
        method: 'GET',
        path: '/api/user/listNotifications',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.listNotifications(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'List Notifications',
            tags: ['api', 'user', 'list notifications'],
            validate: {
                query: {
                    accessToken: Joi.string().trim().required()

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }

        }
    },

    {
        method: 'PUT',
        path: '/api/user/changePushNotificationSettings',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.changePushNotificationSettings(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Turn on/off Notifications',
            tags: ['api', 'user', 'turn on/off notifications'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    pushNotifications:Joi.string().trim().required()

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }

        }
    },

    {
        method: 'GET',
        path: '/api/user/getProfile',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.getProfile(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get Profile',
            tags: ['api', 'user', 'get profile'],
            validate: {
                query: {
                    accessToken: Joi.string().trim().required()

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }

        }
    },

    {
        method: 'GET',
        path: '/api/user/listAboutBPNIandT&C',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.listAboutBPNI(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'List Notifications',
            tags: ['api', 'user', 'list notifications'],
            validate: {
                query: {
                    accessToken: Joi.string().trim().required()

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }

        }
    },

    {
        method: 'PUT',
        path: '/api/user/updateRadius',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.updateRadius(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Update Radius',
            tags: ['api', 'user', 'update radius'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    radius:Joi.string().trim().required()

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }

        }
    },

    {
        method: 'PUT',
        path: '/api/user/forgotPassword',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.forgotPassword(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess("New Password has been sent to your registered email address", data))
                }
            });
        },
        config: {
            description: 'Forgot Password',
            tags: ['api', 'user', 'forgot password'],
            validate: {
                payload: {
                    email: Joi.string().trim().required()

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }

        }
    },

    {
        method: 'POST',
        path: '/api/user/donateMoney',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.donateMoney(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess("New Password has been sent to your registered email address", data))
                }
            });
        },
        config: {
            description: 'Donat Money by user',
            tags: ['api', 'user', 'donate user'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    amount:Joi.number().required()

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }

        }
    },


];