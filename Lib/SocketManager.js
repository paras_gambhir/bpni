'use strict';
/**
 * Created by prince on 12/7/15.
 */
var Config = require('../Config');
var TokenManager = require('./TokenManager');
var Service = require('../Services');
var NotificationManager = require('./NotificationManager');
var async = require('async');

var arr = [];
var listen_id = "12234cfgbd";

var UniversalFunctions = require('../Utils/UniversalFunctions');
//arr[0] = {};

var socket =null;

exports.connectSocket = function (server) {
    if (!server.app) {
        server.app = {}
    }
    server.app.socketConnections = {};
    socket = require('socket.io').listen(server.listener);

    socket.on('disconnect', function(){
        console.log('socket disconnected')
    });

    socket.on('connection', function(socket){
        socket.on('sendMessage',function(data){
            console.log("..........................socket message.....................",data);
            if(data.to && data.from && data.message && data.timeStamp){
                saveMeassage(data,function(err,result){
                    if(err){
                        console.log(err);
                    }else{
                        console.log(".............result.........",result);
                    }
                })
            }else{
                // error format not correct
                console.log("data not in format");
            }
        })

        socket.on('deliveredConfirmation',function(data){
            console.log(".............deliveredConfirmation.................",data);
            if(data._id && data.to && data.from){
                meassgeConformationStatus(data,function(err,result){
                    if(err){
                        console.log(err);
                    }else{
                        console.log(".............result.........",result);
                    }
                })
            }else{
                // error format not correct
                console.log("data not in format");
            }
        })
        socket.on('readConfirmation',function(data){
            console.log(".............sendMessage.................",data);
            if(data._id){
                readConformationStatus(data,function(err,result){
                    if(err){
                        console.log(err);
                    }else{
                        console.log(".............result.........",result);
                    }
                })
            }else{
                // error format not correct
                console.log("data not in format");
            }
        })


        socket.on('typing',function(data){
            console.log(".............typing.................",data);
            if(data.to && data.from){
                typing(data,function(err,result){
                    if(err){
                        console.log(err);
                    }else{
                        console.log(".............result.........",result);
                    }
                })
            }else{
                // error format not correct
                console.log("data not in format");
            }
        })

        socket.on('stopTyping',function(data){
            if(data.to && data.from){
                stopTyping(data,function(err,result){
                    if(err){
                        console.log(err);
                    }else{
                        console.log(".............result.........",result);
                    }
                })
            }else{
                // error format not correct
                console.log("data not in format");
            }
        })

        socket.on('allReadUpdate',function(data){
            console.log("...........all.............read.............",data);
            if(data.to && data.from){
                readAll(data,function(err,result){
                    if(err){
                        console.log(err);
                    }else{
                        console.log(".,.....result..................",result);
                    }
                })
            }else{
                console.log("...........data not in format.,.");
            }
        })
        

        socket.on('messageFromClient', function (data) {
            //Update SocketConnections
            if (data && data.token) {
                TokenManager.decodeToken(data.token, function (err, decodedData) {
                    if (!err && decodedData.id) {
                        if (server.app.socketConnections.hasOwnProperty(decodedData.id)) {
                            server.app.socketConnections[decodedData.id].socketId = socket.id;
                            socket.emit('messageFromServer', { message:'Added To socketConnections',performAction:'INFO'});
                        } else {
                            server.app.socketConnections[decodedData.id] = {
                                socketId: socket.id
                            };
                            socket.emit('messageFromServer', { message:'Socket id Updated',performAction:'INFO'});
                        }
                    } else {
                        socket.emit('messageFromServer', { message:'Invalid Token',performAction:'INFO'});
                    }
                })
            }else {
                console.log('msgFromClient',data)
            }
        });
     //   socket.emit('messageFromServer', { message:'WELCOME TO NERVERMYND', performAction:'INFO'});
        process.on('yourCustomEvent', function (data) {
            if (data['yourConditionHere']){
                var sparkIdToSend = server.app.socketConnections[data['userId'].toString()]
                    && server.app.socketConnections[data['userId']].socketId;
                //GetsocketId
                if (sparkIdToSend) {
                    socket.to(sparkIdToSend).emit('messageFromServer', {
                        message: 'ANY_KIND_OF_MESSAGE_HERE',
                        performAction : 'INFO'
                    });
                } else {
                    console.log('Socket id not found')
                }
            }else {
                console.log('User id not found')
            }
        });
    });

    

    process.on(listen_id,function(data){
           socket.emit(listen_id,data);
         arr.shift();
    })

};

var saveMeassage = function(data,callback){
    var id;
    var len;
    var ToStatus;
    var ToDeviceToken;
    var FromStatus;
    var FromDeviceToken;
    var ToDeviceType;
    var FromDeviceType;
    async.auto({
        saveSms:function(cb){
            data.logs = {
                status:Config.APP_CONSTANTS.DATABASE.MEG_STATUS.REACHSERVER
            }
            data.status = Config.APP_CONSTANTS.DATABASE.MEG_STATUS.REACHSERVER;
            Service.ChatServices.createChats(data,function(err,result){
                if(err){
                    cb(err);
                }else{
                      id = result._id;
                    cb(null);
                }
            })
        },
        checkToOnline:function(cb){
            console.log("./...................data............................",data.to);
            var query = {_id:data.to};
            var options = {lean:true};
            var projections = {
                status:1,
                deviceToken:1,
                deviceType:1
            }
            Service.UsersService.getUsers(query,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                  //  ToStatus =  result[0].status;
                    
                    console.log("..............userId............................",data.to);
                    console.log("........get Device token ...................",err,result);
                    
                    if(result.length){
                        ToDeviceToken = result[0].deviceToken;
                        ToDeviceType = result[0].deviceType
                    }
                    cb(null);
                }
            })
        },
        checkFromOnline:function(cb){
            var query = {_id:data.from};
            var options = {lean:true};
            var projections = { 
                status:1,
                deviceToken:1,
                deviceType:1,
                firstName:1
            }
            Service.UsersService.getUsers(query,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
             /*       if(FromStatus.length){
                        FromStatus =  result[0].status;
                        FromDeviceToken = result[0].deviceToken;
                        FromDeviceType = result[0].deviceType;
                    }*/
                    cb(null);
                }
            })
        },
        sendReachSocket:['saveSms','checkFromOnline','checkToOnline',function(cb){
            var temp;
            temp = data;
            temp.status = "REACHSERVER";
            var process = "status_"+data.from; // message id 
            data._id = id;
            socket.emit(process,temp);
            cb(null);
        }],
        sendMessage:['saveSms','checkFromOnline','checkToOnline','sendReachSocket',function(cb){
       
            console.log("...........ToDeviceType..........",ToDeviceType);
            if(ToDeviceType == "ANDROID"){
                    var temp;
                    temp = data;
                    temp.status = "MESSAGE";
                console.log(".........temp.......................................",temp);
                console.log("........................ToDeviceToken...............",ToDeviceToken);
                    UniversalFunctions.sendAndroidPushNotification(ToDeviceToken,temp,"NEWMESSAGE",function(err,result){
                        if(err){
                            cb(null)
                        }else{
                            cb(null)
                        }
                    })
                cb(null);
                }else if(ToDeviceType == "IOS"){
                cb(null);
                }else {
                //  cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.STATUS_MSG.IMP_ERROR);
                console.log(".......not receive device type,",ToDeviceType);
                cb(null)
            }
        }],
        sendSocketMessage:['saveSms','checkFromOnline','checkToOnline',function(cb){
            console.log(".................data.......................");
            listen_id = "message_"+data.to
            data._id = id;
            console.log("............send.....message........................",listen_id);
            socket.emit(listen_id,data);
            cb(null)
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,null)
        }
    })
}


var meassgeConformationStatus = function(data,callback){
    async.auto({
        updateDb:function(cb){
            
            
            var query = {
                _id:data._id
            }
            
            var setData = {
                status:"DELIVERED"
            }
            
            var options = {lean:true};
            
            Service.UsersService.updateUsers(query,setData,options,function(err,result){
             console.log(".........................update fat in db deleivedd............");
                if(err){
                    cb(err);
                }else{
                    cb(null);
                }
            })
        },
        sendSentStatus:function(cb){
            data.status = "DELIVERED";
            var process = "status_"+data.to; // message id
             socket.emit(process,data);
        }
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null);
        }
    })
}

var readConformationStatus = function(data,callback){
    async.auto({
        saveDb:function(cb){
            var query = {
                _id:data._id
            }
            var setData = {
                status:"READ"
            }
            var options = {lean:true};
            Service.UsersService.updateUsers(query,setData,options,function(err,result){
                if(err){
                    
                    console.log("db error");
                    cb(err);
                }else{
                    cb(null);
                }
            })
        },
        sendSentStatus:function(cb){
            data.status = "READ"
            var process = "status_"+data.to; // message id
            
            console.log(".............read propcesss........................",data);
            socket.emit(process,data);
        }
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null)
        }
    })
}


var typing = function(data,callback){
    data.status = "TYPING";
    data._id = data.from;
    var process = "status_"+data.to;
    
    var tempTo = data.to;
    var tempFrom  = data.from;

    data.to = tempFrom;
    data.from = tempTo;
    
    
    socket.emit(process,data);
}

var stopTyping = function(data,callback){
    data.status = "STOPTYPING";
    data._id = data.from;
    var process = "status_"+data.to;
    socket.emit(process,data);
}

var readAll = function(data,callback){
    async.auto({
        updateMsg:function(cb){
            var logs = {
                status:Config.APP_CONSTANTS.DATABASE.MEG_STATUS.READ
            }
            var query = {to:data.to,
                        from:data.from
            };
            var options = {
                multi:true
            }
            var setData = {
                status  : Config.APP_CONSTANTS.DATABASE.MEG_STATUS.READ,
                logs:{$push:logs}
            }
            Service.ChatServices.updateChats(query,setData,options,function(err,result){
             console.log("............err.................result......................",err,result);
                if(err){
                    cb(err);
                }else{
                    cb(null)
                }
            })
        },
        updateSocket:['updateMsg',function(cb){
            data.status = "ALL_READ";
            var process = "status_"+data.to;
            var temp = data.from;
            data.from = data.to;
            data.to = temp;
            socket.emit(process,data);
            cb(null,null) 
        }]

    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,result)
        }
    })
}


