/**
 * Created by paras on 10/7/15.
 */
module.exports  = {
    AdminController : require('./AdminController'),
    AppVersionController : require('./AppVersionController'),
    UserController:require('./UserController')
};