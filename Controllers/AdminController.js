'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');

var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var NotificationManager = require('../Lib/NotificationManager');
var _ = require('lodash');

var adminLogin = function (payloadData, callback) {

    var adminData;
    var accessToken;
    async.auto({
        checkForEmail: function (cb) {
            var criteria = {
                email: payloadData.email,
                password: UniversalFunctions.CryptData(payloadData.password)
            };
            var projection = {
                password: 0
            };
            var option = {
                lean: true
            };
            console.log("UniversalFunctions.CryptData(payloadData.password)", UniversalFunctions.CryptData(payloadData.password))
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    if (result.length) {
                        adminData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_USER_PASS);
                    }
                }
            });
        },
        setToken: ['checkForEmail', function (cb) {
            var tokenData = {
                id: adminData._id,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        adminData.accessToken = accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {adminData: adminData});
        }
    })

};


var listUsers = function (payloadData, callback) {

    var userData;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    //console.log(result);
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        listUsers: ['checkForAccessToken', function (cb) {
            var criteria = {
                isBlocked: false
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                   // console.log(result);
                    userData = result;
                    cb();
                }
            });

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {userData: userData});
        }
    })

};


var violationReports = function (payloadData, callback) {

    var violationReportsData;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        violationReports: ['checkForAccessToken', function (cb) {
            var criteria = {};
            var projection = {};
            var option = {
                lean: true
            };
            var populate = [
                {
                    path: "User",
                    match: {},
                    select: 'name email',
                    options: {lean: true}
                }
            ];
            Service.ViolationReportServices.getViolationReportsPopulate(criteria, projection, option, populate, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    violationReportsData = result;
                    cb();
                }
            });

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {violationReportsData: violationReportsData});
        }
    })

};


var violationPoints = function (payloadData, callback) {
    var violationPointsData;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        violationPoints: ['checkForAccessToken', function (cb) {
            var criteria = {
                isDeleted: false
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.ViolationPointsServices.getViolationPoints(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    violationPointsData = result;
                    cb();
                }
            });

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {violationPointsData: violationPointsData});
        }
    })
};


var addViolationPoint = function (payloadData, callback) {

    var violationPoints;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        violationPoints: ['checkForAccessToken', function (cb) {
            var dataToBeInserted = {
                points: payloadData.violationPoint
            };

            Service.ViolationPointsServices.createViolationPoints(dataToBeInserted, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    violationPoints = result;
                    cb();
                }
            });

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {violationPoints: violationPoints});
        }
    })
};


var deleteViolationPoint = function (payloadData, callback) {

    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        violationPoints: ['checkForAccessToken', function (cb) {
            var criteria = {
                _id: payloadData.violationPointId
            };
            var setQuery = {
                isDeleted: true
            };
            Service.ViolationPointsServices.updateViolationPoints(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    console.log(err);
                    console.log(dataAry);
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })
};


var editViolationPoint = function (payloadData, callback) {
    var data;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        violationPoints: ['checkForAccessToken', function (cb) {
            var criteria = {
                _id: payloadData.violationPointId
            };
            var setQuery = {
                points: payloadData.violationPoint
            };
            var options = {
                upsert: true
            };
            Service.ViolationPointsServices.updateViolationPoints(criteria, setQuery, options, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    data = dataAry
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {"violationPoints": data});
        }
    })
};


var getBPNIPosts = function (payloadData, callback) {
    var BPNIPostsData;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        BPNIPosts: ['checkForAccessToken', function (cb) {
            var criteria = {};
            var projection = {};
            var option = {
                lean: true
            };
            Service.BPNIPostsServices.getBPNIPosts(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    BPNIPostsData = result;
                    cb();
                }
            });

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {BPNIPostsData: BPNIPostsData});
        }
    })
};


var addBPNIPosts = function (payloadData, callback) {
    var BPNIPosts = false;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        BPNIPosts: ['checkForAccessToken', function (cb) {
            var criteria = {
                isDeleted: false
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.BPNIPostsServices.getBPNIPosts(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length)
                        BPNIPosts = true;
                    cb();
                }
            });
        }],
        addBPNIPosts: ['BPNIPosts', function (cb) {
            if (BPNIPosts == true) {
                console.log("inside true");
                cb();
            }
            else {
                console.log("dfsdf");
                var dataToBeCreated = {
                    title: payloadData.title,
                    body: payloadData.postBody
                };
                Service.BPNIPostsServices.createBPNIPosts(dataToBeCreated, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        cb();
                    }
                });
            }

        }],
        editBPNIPosts: ['addBPNIPosts', function (cb) {
            if (BPNIPosts == true) {
                var criteria = {};
                var setQuery = {
                    title: payloadData.title,
                    body: payloadData.postBody
                };
                Service.BPNIPostsServices.updateBPNIPosts(criteria, setQuery, {new: true}, function (err, dataAry) {
                    if (err) {
                        cb(err)
                    } else {
                        cb();
                    }
                });
            }
            else {
                cb();
            }

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })
};


var getCounsellors = function (payloadData, callback) {

    var CounsellorData;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        BPNIPosts: ['checkForAccessToken', function (cb) {
            var criteria = {
                isDeleted: false
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CounsellorServices.getCounsellor(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    CounsellorData = result;
                    cb();
                }
            });

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {CounsellorData: CounsellorData});
        }
    })
};


var addCounsellor = function (payloadData, callback) {

    var profilePicURL;
    var counsellor;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        uploadImage: ['checkForAccessToken', function (cb) {
            if (payloadData.pic && payloadData.pic.filename) {

                var randomNo = Math.random();

                UploadManager.uploadFile(payloadData.pic, randomNo, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        profilePicURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                        cb();
                    }
                })
            } else {
                cb();
            }
        }],
        Counsellor: ['uploadImage', function (cb) {
            var dataToBeInserted = {
                name: payloadData.name,
                email: payloadData.email,
                imageUrl: profilePicURL,
                contactNo: payloadData.contactNo,
                latitude: payloadData.latitude,
                longitude: payloadData.longitude,
                address: payloadData.address
            };

            Service.CounsellorServices.createCounsellor(dataToBeInserted, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    counsellor = result;
                    cb();
                }
            });

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {counsellor: counsellor});
        }
    })
};


var editCounsellor = function (payloadData, callback) {

    var profilePicURL;
    var counsellor;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        uploadImage: ['checkForAccessToken', function (cb) {
            if (payloadData.pic && payloadData.pic.filename) {

                var randomNo = Math.random();

                UploadManager.uploadFile(payloadData.pic, randomNo, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        profilePicURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                        cb();
                    }
                })
            } else {
                profilePicURL = payloadData.imageUrl;
                cb();
            }
        }],
        Counsellor: ['uploadImage', function (cb) {
            var criteria = {
                _id: payloadData.counsellorId
            };
            var setQuery = {
                name: payloadData.name,
                contactNo: payloadData.contactNo,
                latitude: payloadData.latitude,
                longitude: payloadData.longitude,
                address: payloadData.address,
                email: payloadData.email,
                imageUrl: profilePicURL
            };
            var options = {
                upsert: true,
                new: true
            };
            Service.CounsellorServices.updateCounsellor(criteria, setQuery, options, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    counsellor = dataAry;
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {counsellor: counsellor});
        }
    })

};


var deleteCounsellor = function (payloadData, callback) {

    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        Counsellor: ['checkForAccessToken', function (cb) {
            var criteria = {
                _id: payloadData.counsellorId
            };
            var setQuery = {
                isDeleted: true
            };
            Service.CounsellorServices.updateCounsellor(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })
};


var listDonations = function (payloadData, callback) {
    var donationData;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        donations: ['checkForAccessToken', function (cb) {
            var criteria = {};
            var projection = {};
            var option = {
                lean: true
            };
            var populate = [
                {
                    path: "User",
                    match: {},
                    select: 'name email',
                    options: {lean: true}
                }
            ];
            Service.DonationServices.getDonationPopulate(criteria, projection, option, populate, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    donationData = result;
                    cb();
                }
            });

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {donationData: donationData});
        }
    })

};


var listPetitions = function (payloadData, callback) {
    var petitionData;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        petitions: ['checkForAccessToken', function (cb) {
            var criteria = {
                isDeleted: false
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.PetitionServices.getPetition(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    petitionData = result;
                    cb();
                }
            });

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {petitionData: petitionData});
        }
    })
};


var addPetition = function (payloadData, callback) {
    var profilePicURL;
    var data;
    var users = [];
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        uploadImage: ['checkForAccessToken', function (cb) {
            if (payloadData.pic && payloadData.pic.filename) {

                var randomNo = Math.random();

                UploadManager.uploadFile(payloadData.pic, randomNo, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        profilePicURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                        cb();
                    }
                })
            } else {
                cb();
            }
        }],
        Petition: ['uploadImage', function (cb) {

            console.log("profile", profilePicURL);
            var dataToBeInserted = {
                name: payloadData.name,
                description: payloadData.description,
                imageUrl: profilePicURL
            };

            Service.PetitionServices.createPetition(dataToBeInserted, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    data = result;
                    cb();
                }
            });

        }],
        updatePetitionNumber: ['Petition', function (cb) {
            var criteria = {};
            var setQuery = {
                $inc: {totalPetitions: 1, totalNotifications: 1}
            };
            var options = {
                multi: true,
                upsert: true
            };
            Service.UserServices.updateMulti(criteria, setQuery, options, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    //  console.log(dataAry);
                    cb();
                }
            });
        }],
        getAllUsers: ['updatePetitionNumber', function (cb) {

            var criteria = {
                isBlocked: false
            };
            var projection = {
                deviceToken: 1,
                deviceType: 1,
                totalPetitions: 1,
                totalNewEvents: 1,
                totalNotifications: 1
            };
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    if (result.length) {
                        users = result;
                        console.log("users..", users);
                        cb();
                    }
                    else {
                        console.log("dfdnndf")
                        cb();
                    }
                }
            });

        }],
        sendNotification: ['getAllUsers', function (cb) {

            for (var i = 0; i < users.length; i++) {
                (function (i) {
                    var message = {
                        "data": {
                            "totalPetitions": users[i].totalPetitions,
                            "totalNewEvents": users[i].totalNewEvents,
                            "totalNotifications": users[i].totalNotifications
                        },
                        "message": "A new petition has been added"
                    };

                    if (users[i].deviceType == "ANDROID") {
                        NotificationManager.sendAndroidPushNotification(users[i].deviceToken,message.message, message.data, function (err, result) {
                            if (err) {
                                console.log(err)
                            } else {
                                console.log(result);
                            }
                        })
                    }
                    else {
                        NotificationManager.sendIosPushNotification(users[i].deviceToken, "A new petition has been added", message, function (err, result) {
                            if (err) {
                                console.log(err)
                            } else {
                                console.log(result);
                            }
                        })
                    }


                }(i))
            }
            cb();
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {"petitions": data});
        }
    })

};


var editPetition = function (payloadData, callback) {
    var profilePicURL;
    var data;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        uploadImage: ['checkForAccessToken', function (cb) {
            if (payloadData.pic && payloadData.pic.filename) {

                var randomNo = Math.random();

                UploadManager.uploadFile(payloadData.pic, randomNo, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        profilePicURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                        cb();
                    }
                })
            } else {
                profilePicURL = payloadData.imageUrl;
                cb();
            }
        }],
        Petition: ['uploadImage', function (cb) {
            var criteria = {
                _id: payloadData.petitionId
            };
            var setQuery = {
                name: payloadData.name,
                description: payloadData.description,
                imageUrl: profilePicURL
            };
            var options = {
                upsert: true
            };
            Service.PetitionServices.updatePetition(criteria, setQuery, options, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    console.log("fsdf", dataAry);
                    data = dataAry;
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {"petition": data});
        }
    })

};


var deletePetition = function (payloadData, callback) {

    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        Petition: ['checkForAccessToken', function (cb) {
            var criteria = {
                _id: payloadData.petitionId
            };
            var setQuery = {
                isDeleted: true
            };
            Service.PetitionServices.updatePetition(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })
};


var listSignedPetitions = function (payloadData, callback) {
    var signedPetitionData;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        signedPetitions: ['checkForAccessToken', function (cb) {
            var criteria = {};
            var projection = {};
            var option = {
                lean: true,
                sort: {Petition: 1}
            };
            var populate = [
                {
                    path: "User",
                    match: {},
                    select: 'name email',
                    options: {lean: true}
                },
                {
                    path: "Petition",
                    match: {},
                    select: 'name imageUrl',
                    options: {lean: true}
                }
            ];
            Service.SignedPetitionServices.getSignedPetitionsPopulate(criteria, projection, option, populate, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    signedPetitionData = result;
                    cb();
                }
            });

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {signedPetitionData: signedPetitionData});
        }
    })

};


var listCounsellorRequests = function (payloadData, callback) {
    var counsellorRequests = [];
    var hello = [];
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        counsellorRequests: ['checkForAccessToken', function (cb) {
            var criteria = {};
            var projection = {};
            var option = {
                lean: true
            };
            var populate = [
                {
                    path: "User",
                    match: {},
                    select: 'name ',
                    options: {lean: true}
                }
            ];
            Service.CounsellorRequestsServices.getCounsellorRequestsPopulate(criteria, projection, option, populate, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    counsellorRequests = result;
                    cb();
                }
            });

        }],
        getUniqueResults:['counsellorRequests',function(cb)
        {


            hello = _.uniqBy(counsellorRequests, 'email');
            cb();
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {counsellorRequests: hello});
        }
    })
};


var listNewEvents = function (payloadData, callback) {
    var newEventsData;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        newEvents: ['checkForAccessToken', function (cb) {
            var criteria = {
                isDeleted: false
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.NewEventsServices.getNewEvents(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    newEventsData = result;
                    cb();
                }
            });

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {newEventsData: newEventsData});
        }
    })
};


var addNewEvent = function (payloadData, callback) {
    var newEvent;
    var users = [];
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        NewEvent: ['checkForAccessToken', function (cb) {

            var dataToBeInserted = {
                text: payloadData.text,
                title: payloadData.title
            };

            Service.NewEventsServices.createNewEvents(dataToBeInserted, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    newEvent = result;
                    cb();
                }
            });

        }],
        updateEventNumber: ['NewEvent', function (cb) {
            var criteria = {};
            var setQuery = {
                $inc: {totalNewEvents: 1, totalNotifications: 1}

            };
            var options = {
                multi: true,
                upsert: true
            };
            Service.UserServices.updateMulti(criteria, setQuery, options, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    console.log(dataAry);
                    cb();
                }
            });
        }],
        getAllUsers: ['updateEventNumber', function (cb) {

            var criteria = {
                isBlocked: false
            };
            var projection = {
                deviceToken: 1,
                deviceType: 1,
                totalPetitions: 1,
                totalNewEvents: 1,
                totalNotifications: 1
            };
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    if (result.length) {
                        users = result;
                        console.log("users..", users);
                        cb();
                    }
                    else {
                        console.log("dfdnndf")
                        cb();
                    }
                }
            });

        }],
        sendNotification: ['getAllUsers', function (cb) {

            for (var i = 0; i < users.length; i++) {
                (function (i) {
                    var message = {
                        "data": {
                            "totalPetitions": users[i].totalPetitions,
                            "totalNewEvents": users[i].totalNewEvents,
                            "totalNotifications": users[i].totalNotifications
                        },
                        "message": "A new event has been added"
                    };

                    if (users[i].deviceType == "ANDROID") {
                        NotificationManager.sendAndroidPushNotification(users[i].deviceToken,message.message, message.data, function (err, result) {
                            if (err) {
                                console.log(err)
                            } else {
                                console.log(result);
                            }
                        })
                    }
                    else {
                        NotificationManager.sendIosPushNotification(users[i].deviceToken, "A new event has been added", message, function (err, result) {
                            if (err) {
                                console.log(err)
                            } else {
                                console.log(result);
                            }
                        })
                    }


                }(i))
            }
            cb();
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {newEvent: newEvent});
        }
    })
};

Array.prototype.uniqueObjectArray = function(field,array) {
    var processed = [];
    for (var i=array.length-1; i>=0; i--) {
        if (array[i].hasOwnProperty(field)) {
            if (processed.indexOf(array[i][field])<0) {
                processed.push(array[i][field]);
            } else {
                this.splice(i, 1);
            }
        }
    }
}


var editNewEvent = function (payloadData, callback) {
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        event: ['checkForAccessToken', function (cb) {
            var criteria = {
                _id: payloadData.eventId
            };
            var setQuery = {
                text: payloadData.text,
                title: payloadData.title
            };

            var options = {
                new: true,
                upsert: true
            };
            Service.NewEventsServices.updateNewEvents(criteria, setQuery, options, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })

};


var deleteNewEvent = function (payloadData, callback) {

    console.log("bwhfd", payloadData);
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        event: ['checkForAccessToken', function (cb) {
            var criteria = {
                _id: payloadData.eventId
            };
            var setQuery = {
                isDeleted: true
            };
            Service.NewEventsServices.updateNewEvents(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })
};


var editAboutBPNI = function (payloadData, callback) {
    var adminData;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        adminData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        editAboutBPNI: ['checkForAccessToken', function (cb) {
            var criteria = {};
            var setQuery = {
                aboutBPNI: payloadData.aboutBPNI,
                tAndC: payloadData.tAndC,
                donationText: payloadData.donationText
            };
            Service.AdminServices.updateMulti(criteria, setQuery, {new: true, multi: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })
};


var listAboutBPNI = function (payloadData, callback) {
    var adminData;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        adminData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        }

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {adminData: adminData});
        }
    })
};


var changePassword = function (payloadData, callback) {
    var adminData;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        adminData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        checkForPassword: ['checkForAccessToken', function (cb) {
            var criteria = {
                _id: adminData._id,
                password: UniversalFunctions.CryptData(payloadData.oldPassword)
            };
            Service.AdminServices.getAdmin(criteria, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    console.log(dataAry)
                    if (dataAry.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_OLD_PASSWORD);
                    }

                }
            });
        }],
        changePassword: ['checkForPassword', function (cb) {
            var criteria = {
                _id: adminData._id
            };
            var setQuery = {
                password: UniversalFunctions.CryptData(payloadData.newPassword)
            };
            Service.AdminServices.updateAdmin(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })
};


module.exports = {

    adminLogin: adminLogin,
    listUsers: listUsers,
    violationReports: violationReports,
    violationPoints: violationPoints,
    addViolationPoint: addViolationPoint,
    deleteViolationPoint: deleteViolationPoint,
    editViolationPoint: editViolationPoint,
    getBPNIPosts: getBPNIPosts,
    addBPNIPosts: addBPNIPosts,
    getCounsellors: getCounsellors,
    addCounsellor: addCounsellor,
    editCounsellor: editCounsellor,
    deleteCounsellor: deleteCounsellor,
    listDonations: listDonations,
    listPetitions: listPetitions,
    addPetition: addPetition,
    editPetition: editPetition,
    deletePetition: deletePetition,
    listSignedPetitions: listSignedPetitions,
    listCounsellorRequests: listCounsellorRequests,
    listNewEvents: listNewEvents,
    addNewEvent: addNewEvent,
    editNewEvent: editNewEvent,
    deleteNewEvent: deleteNewEvent,
    editAboutBPNI: editAboutBPNI,
    listAboutBPNI: listAboutBPNI,
    changePassword: changePassword

};