/**
 * Created by cbl34 on 15/9/16.
 */
'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');


var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var NotificationManager = require('../Lib/NotificationManager');
var CodeGenerator = require('../Lib/CodeGenerator');
var moment = require('moment');
var emailPassword = require('../Lib/email');


var signupWithEmail = function (payloadData, callback) {

    var accessToken = null;
    var userFound = false;
    var userData;
    var BPNIPosts;
    async.auto({
        checkForEmail: function (cb) {
            var criteria = {
                email: payloadData.email
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userFound = true;
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST);

                    }
                    else {
                        cb();

                    }
                }
            });

        },
        insertUser: ['checkForEmail', function (cb) {
            var dataToBeInserted = {
                name: payloadData.name,
                gender: payloadData.gender ? payloadData.gender: "",
                age: payloadData.age ? payloadData.age: 0,
                latitude: payloadData.latitude,
                longitude: payloadData.latitude,
                password: UniversalFunctions.CryptData(payloadData.password),
                passwordNotEncrypted:payloadData.password,
                address: payloadData.address,
                email: payloadData.email,
                deviceType: payloadData.deviceType,
                deviceToken: payloadData.deviceToken,
                profilePicUrl: {
                    original: 'https://s3-us-west-2.amazonaws.com/vippy/default-person.jpg',
                    thumbnail: 'https://s3-us-west-2.amazonaws.com/vippy/default-person.jpg'

                }
            };
            Service.UserServices.createUser(dataToBeInserted, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    userData = result;
                    cb();
                }
            });
        }],
        setToken: ['insertUser', function (cb) {
            var tokenData = {
                id: userData._id,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.USER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        userData.accessToken = accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }],
        listBPNIPosts: ['setToken', function (cb) {
            var criteria = {
                isDeleted: false
            };
            Service.BPNIPostsServices.getBPNIPosts(criteria, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    BPNIPosts = dataAry;
                    cb();
                }
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {userData: userData, BPNIPosts: BPNIPosts});
        }
    })
};


var createFBUser = function (payloadData, callback) {
    var accessToken = null;
    var userFound = false;
    var userData;

    var BPNIPosts;
    async.auto({
        checkForEmail: function (cb) {
            var criteria = {
                fbId: payloadData.fbId
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userFound = true;
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST);

                    }
                    else {
                        var criteria = {
                            email: payloadData.email
                        };
                        var projection = {};
                        var option = {
                            lean: true
                        };
                        Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                            if (err) {
                                cb(err)
                            } else {
                                if (result.length) {
                                    userFound = true;
                                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST);

                                }
                                else {
                                    cb();
                                }
                            }
                        });

                    }
                }
            });

        },
        insertUser: ['checkForEmail', function (cb) {

            var dataToBeInserted = {
                name: payloadData.name ? payloadData.name : "",
                gender: payloadData.gender ? payloadData.gender : "",
                age: payloadData.age ? payloadData.age : 0,
                email: payloadData.email,
                latitude: (payloadData.latitude && payloadData.latitude !="")  ? payloadData.latitude : 0,
                longitude:(payloadData.longitude && payloadData.longitude !="") ? payloadData.longitude : 0,
                address: (payloadData.address && payloadData.address != 0) ? payloadData.address: "",
                deviceType: payloadData.deviceType,
                deviceToken: payloadData.deviceToken,
                profilePicURL: {
                    "thumbnail": "http://graph.facebook.com/"+payloadData.fbId+"/picture?type=large",
                    "original": "http://graph.facebook.com/"+payloadData.fbId+"/picture?type=normal"
                },
                fbId: payloadData.fbId
            };
            console.log(dataToBeInserted);
            Service.UserServices.createUser(dataToBeInserted, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    userData = result;
                    cb();
                }
            });
        }],
        setToken: ['insertUser', function (cb) {
            var tokenData = {
                id: userData._id,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.USER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        userData.accessToken = accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }],
        listBPNIPosts: ['setToken', function (cb) {
            var criteria = {
                isDeleted: false
            };
            Service.BPNIPostsServices.getBPNIPosts(criteria, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    BPNIPosts = dataAry;
                    cb();
                }
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {userData: userData, BPNIPosts: BPNIPosts});
        }
    })
};


var fbLogin = function (payloadData, callback) {
    var userData;
    var accessToken;
    var BPNIPosts;
    async.auto({
        checkForFbId: function (cb) {
            var criteria = {
                fbId: payloadData.fbId
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACCOUNT_NOT_REGISTERED);
                    }
                }
            });
        },
        updateUser: ['checkForFbId', function (cb) {
            var criteria = {
                id: userData._id
            };
            var setQuery = {
                deviceToken: payloadData.deviceToken,
                deviceType: payloadData.deviceType
            };
            Service.UserServices.updateUser(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });

        }],
        setToken: ['updateUser', function (cb) {
            var tokenData = {
                id: userData._id,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.USER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        userData.accessToken = accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }],
        listBPNIPosts: ['setToken', function (cb) {
            var criteria = {
                isDeleted: false
            };
            Service.BPNIPostsServices.getBPNIPosts(criteria, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    BPNIPosts = dataAry;
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {userData: userData, BPNIPosts: BPNIPosts});
        }
    })

};


var emailLogin = function (payloadData, callback) {
    var userData;
    var accessToken;
    var BPNIPosts;
    async.auto({
        checkForEmail: function (cb) {
            var criteria = {
                email: payloadData.email,
                password: UniversalFunctions.CryptData(payloadData.password)
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_USER_PASS);
                    }
                }
            });
        },
        updateUser: ['checkForEmail', function (cb) {
            var criteria = {
                _id: userData._id
            };
            var setQuery = {
                deviceToken: payloadData.deviceToken,
                deviceType: payloadData.deviceType,
                passwordNotEncrypted:payloadData.password
            };
            Service.UserServices.updateUser(criteria, setQuery, {upsert: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });

        }],
        setToken: ['updateUser', function (cb) {
            var tokenData = {
                id: userData._id,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.USER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        userData.accessToken = accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }],
        listBPNIPosts: ['setToken', function (cb) {
            var criteria = {
                isDeleted: false
            };
            Service.BPNIPostsServices.getBPNIPosts(criteria, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    BPNIPosts = dataAry;
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {userData: userData, BPNIPosts: BPNIPosts});
        }
    })
};


var accessTokenLogin = function (payloadData, callback) {

    var BPNIPosts;
    var userData;
    var accessToken;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        updateUser: ['checkForAccessToken', function (cb) {
            var criteria = {
                id: userData._id
            };
            var setQuery = {
                deviceToken: payloadData.deviceToken,
                deviceType: payloadData.deviceType
            };
            Service.UserServices.updateUser(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });

        }],
        setToken: ['updateUser', function (cb) {
            var tokenData = {
                id: userData._id,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.USER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        userData.accessToken = accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }],
        listBPNIPosts: ['setToken', function (cb) {
            var criteria = {
                isDeleted: false
            };
            Service.BPNIPostsServices.getBPNIPosts(criteria, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    BPNIPosts = dataAry;
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {userData: userData, BPNIPosts: BPNIPosts});
        }
    })

};


var logout = function (payloadData, callback) {

    var userData;
    var accessToken;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        updateUser: ['checkForAccessToken', function (cb) {
            var criteria = {
                id: userData._id
            };
            var setQuery = {
                accessToken: ''
            };
            Service.UserServices.updateUser(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })

};


var reportViolation = function (payloadData, callback) {

    var userId;
    var profilePicURL = '';
    var profilePicURL1 = '';
    var profilePicURL2 = '';
    var profilePicURL3 = '';
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id: 1
            };
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        uploadImage: ['checkToken', function (cb) {
            if (payloadData.pic && payloadData.pic.filename) {
                var randomNo = Math.random();
                UploadManager.uploadFile(payloadData.pic, randomNo, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        profilePicURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                        cb();
                    }
                })
            }
            else{
                cb();
            }
        }],
        uploadImage1: ['uploadImage', function (cb) {
            if (payloadData.pic1 && payloadData.pic1.filename) {
                var randomNo1 = Math.random();
                UploadManager.uploadFile(payloadData.pic1, randomNo1, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        profilePicURL1 = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                        cb();
                    }
                })
            }
            else{
                cb();
            }
        }],
        uploadImage2: ['uploadImage1', function (cb) {

            if (payloadData.pic2 && payloadData.pic2.filename) {
                var randomNo2 = Math.random();
                UploadManager.uploadFile(payloadData.pic2, randomNo2, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        profilePicURL2 = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                        cb();
                    }
                })
            }
            else{
                cb();
            }
        }],
        uploadImage3: ['uploadImage2', function (cb) {

            if (payloadData.pic3 && payloadData.pic3.filename) {
                var randomNo3 = Math.random();
                UploadManager.uploadFile(payloadData.pic3, randomNo3, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        profilePicURL3 = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                        cb();
                    }
                })
            }
            else{
                cb();
            }
        }],
        insertViolationReport: ['uploadImage3', function (cb) {

            var dataToBeInserted = {
                title: payloadData.title,
                subject: payloadData.subject,
                insertedDate: payloadData.publishedDate,
                textBox: payloadData.textBox,
                imageUrl: profilePicURL,
                imageUrl1: profilePicURL1,
                imageUrl2: profilePicURL2,
                imageUrl3: profilePicURL3,
                User: userId
            };
            console.log(dataToBeInserted);
            Service.ViolationReportServices.createViolationReports(dataToBeInserted, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })

};


var listViolationPoints = function (payloadData, callback) {
    var violationPoints;
    var accessToken;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        listViolationPoints: ['checkForAccessToken', function (cb) {
            var criteria = {
                isDeleted: false
            };
            Service.ViolationPointsServices.getViolationPoints(criteria, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    violationPoints = dataAry;
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {violationPoints: violationPoints});
        }
    })
};


var listBPNIPosts = function (payloadData, callback) {
    var BPNIPosts;
    var accessToken;
    var totalNotifications;
    var totalPetitions;
    var totalNewEvents;
    var donationText;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        totalNewEvents = result[0].totalNewEvents;
                        totalPetitions = result[0].totalPetitions;
                        totalNotifications = result[0].totalNotifications;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        listBPNIPosts: ['checkForAccessToken', function (cb) {
            var criteria = {
                isDeleted: false
            };
            Service.BPNIPostsServices.getBPNIPosts(criteria, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    BPNIPosts = dataAry;
                    cb();
                }
            });
        }],
        getDonationText: ['checkForAccessToken', function (cb) {
            var criteria = {};
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria,projection,option, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    console.log(dataAry);
                    donationText = dataAry[0].donationText;
                    cb();
                }
            });
        }],

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                BPNIPosts: BPNIPosts,
                totalNotifications: totalNotifications,
                totalPetitions: totalPetitions,
                totalNewEvents: totalNewEvents,
                donationText:donationText
            });
        }
    })

};


var listCounsellors = function (payloadData, callback) {
    var Counsellors;
    var accessToken;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        listCounsellors: ['checkForAccessToken', function (cb) {
            var criteria = {
                isDeleted: false
            };
            Service.CounsellorServices.getCounsellor(criteria, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    Counsellors = dataAry;
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {Counsellors: Counsellors});
        }
    })

};


var listPetitions = function (payloadData, callback) {
    var Petitions;
    var SignedPetitions;
    var finalPetitions = [];
    var totalNotifications;
    var totalPetitions;
    var userId;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        totalNotifications = result[0].totalNotifications;
                        totalPetitions = result[0].totalPetitions;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        listPetitions: ['checkForAccessToken', function (cb) {
            var criteria = {
                isDeleted: false
            };
            Service.PetitionServices.getPetition(criteria, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    Petitions = dataAry;
                    cb();
                }
            });
        }],
        listSignedPetitions: ['checkForAccessToken', function (cb) {
            var criteria = {
                User: userId
            };
            Service.SignedPetitionServices.getSignedPetition(criteria, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    SignedPetitions = dataAry;
                    cb();
                }
            });
        }],
        matchPetitions: ['listPetitions', 'listSignedPetitions', function (cb) {

            if(!Petitions.length)
            {
                cb();
            }
            else{
                for (var i = 0; i < Petitions.length; i++) {
                    (function (i) {

                        if (SignedPetitions.length == 0) {
                            finalPetitions.push({
                                "_id": Petitions[i]._id,
                                "isDeleted": Petitions[i].isDeleted,
                                "description": Petitions[i].description,
                                "date": Petitions[i].date,
                                "name": Petitions[i].name,
                                "image": Petitions[i].imageUrl,
                                "signedUp": false
                            });
                            if (i == Petitions.length - 1) {
                                cb();
                            }
                        }
                        else {
                            for (var j = 0; j < SignedPetitions.length; j++) {
                                (function (j) {
                                    if (Petitions[i]._id.toString() == SignedPetitions[j].Petition.toString()) {
                                        console.log("inside");

                                        if (j == SignedPetitions.length - 1) {
                                            finalPetitions.push({
                                                "_id": Petitions[i]._id,
                                                "isDeleted": Petitions[i].isDeleted,
                                                "description": Petitions[i].description,
                                                "date": Petitions[i].date,
                                                "name": Petitions[i].name,
                                                "image": Petitions[i].imageUrl,
                                                "signedUp": true
                                            });
                                            if (i == Petitions.length - 1) {
                                                cb();
                                            }
                                        }
                                    }
                                    else {

                                        if (j == SignedPetitions.length - 1) {
                                            finalPetitions.push({
                                                "_id": Petitions[i]._id,
                                                "isDeleted": Petitions[i].isDeleted,
                                                "description": Petitions[i].description,
                                                "date": Petitions[i].date,
                                                "name": Petitions[i].name,
                                                "image": Petitions[i].imageUrl,
                                                "signedUp": false
                                            });
                                            if (i == Petitions.length - 1) {
                                                cb();
                                            }
                                        }
                                    }


                                }(j))
                            }
                        }

                    }(i))

                }
            }


        }],

        checkNotification: ['matchPetitions', function (cb) {

            if (payloadData.notificationScreen == "true") {
                totalNotifications = parseInt(totalNotifications) - parseInt(totalPetitions);
                var criteria = {
                    _id: userId
                };
                var setQuery = {
                    totalPetitions: 0,
                    totalNotifications: parseInt(totalNotifications)
                };
                var options = {
                    new: true,
                    upsert: true
                };
                Service.UserServices.updateUser(criteria, setQuery, options, function (err, dataAry) {
                    if (err) {
                        cb(err)
                    } else {
                        //  console.log(dataAry);
                        cb();
                    }
                });

            }
            else {
                cb();
            }

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {Petitions: finalPetitions});
        }
    })

};


var signupPetition = function (payloadData, callback) {
    var userId;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        signupPetition: ['checkForAccessToken', function (cb) {
            var data = {
                Petition: payloadData.petitionId,
                User: userId
            };
            Service.SignedPetitionServices.createSignedPetition(data, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })

};


var getUserDetails = function (payloadData, callback) {
    var userData;
    var accessToken;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        }

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {userData: userData});
        }
    })
};


var CounsellorRequests = function (payloadData, callback) {
    var userId;
    var profilePicURL;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        uploadImage: ['checkForAccessToken', function (cb) {
            if (payloadData.pic && payloadData.pic.filename) {
                UploadManager.uploadFile(payloadData.pic, userId, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        profilePicURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original || null;
                        cb();
                    }
                })
            } else {
                cb();
            }
        }],
        CounsellorRequests: ['uploadImage', function (cb) {

            console.log(profilePicURL);

            var dataTobeInserted = {
                User: userId,
                name: payloadData.name,
                gender: payloadData.gender,
                age: payloadData.age,
                imageUrl: profilePicURL,
                description: payloadData.description,
                email: payloadData.email,
                contactNo: payloadData.contactNo
            };
            Service.CounsellorRequestsServices.createCounsellorRequests(dataTobeInserted, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })

};


var editProfile = function (payloadData, callback) {
    var userData;
    var newData;
    var profilePicURL;
    var accessToken;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        uploadImage: ['checkForAccessToken', function (cb) {
            if (payloadData.pic && payloadData.pic.filename) {
                UploadManager.uploadFile(payloadData.pic, userData._id, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        profilePicURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                        cb();
                    }
                })
            } else {
                profilePicURL = payloadData.imageUrl;
                cb();
            }
        }],
        updateUser: ['uploadImage', function (cb) {
            var criteria = {
                _id: userData._id
            };
            var setQuery = {
                name: payloadData.name,
                age: payloadData.age,
                gender: payloadData.gender,
                latitude: payloadData.latitude,
                longitude: payloadData.longitude,
                address: payloadData.address,
                profilePicURL: {
                    original: profilePicURL,
                    thumbnail: profilePicURL
                }
            };

            var options = {
                upsert:true
            };
            Service.UserServices.updateUser(criteria, setQuery, options, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    console.log(dataAry);
                    newData = dataAry;
                    newData.email = payloadData.email;
                    cb();
                }
            });

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {userData: newData});
        }
    })

};


var listNotifications = function (payloadData, callback) {
    var Notifications;
    var totalNotifications;
    var totalNewEvents;
    var userId;

    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        totalNotifications = result[0].totalNotifications;
                        totalNewEvents = result[0].totalNewEvents;
                        userId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        listNotifications: ['checkForAccessToken', function (cb) {
            var criteria = {
                isDeleted: false
            };
            Service.NewEventsServices.getNewEvents(criteria, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    Notifications = dataAry;
                    cb();
                }
            });
        }],
        checkNotification: ['listNotifications', function (cb) {

            totalNotifications = parseInt(totalNotifications) - parseInt(totalNewEvents);
            var criteria = {
                _id: userId
            };
            var setQuery = {
                totalNewEvents: 0,
                totalNotifications: parseInt(totalNotifications)
            };
            var options = {
                new: true,
                upsert: true
            };
            Service.UserServices.updateUser(criteria, setQuery, options, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });


        }]


    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {Notifications: Notifications});
        }
    })

};


var changePushNotificationSettings = function (payloadData, callback) {

    console.log("sdsdf", payloadData)
    var userId;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        console.log(userId);
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        changePush: ['checkForAccessToken', function (cb) {
            var criteria = {
                _id: userId
            };
            console.log(payloadData.pushNotifications);
            if (payloadData.pushNotifications == "true") {
                console.log("inside true");
                var setQuery = {
                    pushNotifications: true
                };
            }
            else {
                console.log("inside false");
                var setQuery = {
                    pushNotifications: false
                };
            }

            Service.UserServices.updateUser(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    console.log(dataAry);
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {pushNotifications: payloadData.pushNotifications});
        }
    })
};


var getProfile = function (payloadData, callback) {
    var userData;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        }
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {userData: userData});
        }
    })

};


var listAboutBPNI = function (payloadData, callback) {
    var userData;
    var aboutBpni;
    var termsAndConditions;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        aboutBPNI: ['checkForAccessToken', function (cb) {
            var criteria = {};
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminServices.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        aboutBpni = result[0].aboutBPNI;
                        termsAndConditions = result[0].tAndC;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {aboutBpni: aboutBpni, termsAndConditions: termsAndConditions});
        }
    })


};


var updateRadius = function (payloadData, callback) {

    var userId;
    var radius;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        changePush: ['checkForAccessToken', function (cb) {
            var criteria = {
                _id: userId
            };

            var setQuery = {
                radius: payloadData.radius
            };

            var options = {
                upsert: true,
                new: true
            };

            Service.UserServices.updateUser(criteria, setQuery, options, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {radius: payloadData.radius});
        }
    })
};


var forgotPassword = function(payloadData,callback)
{
    var userId;
    var password;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                email: payloadData.email

            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        password = result[0].passwordNotEncrypted;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
                    }
                }
            });
        },
        sendEmail:['checkForAccessToken',function(cb)
        {
            var subject = "BPNI";
            var content = "It seems that you have forgotten your password.<br>";
            content += "Your password is : " + password + " \n <br>";
            content += "Thank You <br>\n";
            content += "\n\n<br>";
            content += "Team BPNI \n";
            emailPassword.sendEmail(payloadData.email,subject,content);
            cb();

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })

};


var donateMoney = function(payloadData,callback)
{
    var userId;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken

            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserServices.getUsers(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
                    }
                }
            });
        },
        saveDonate:['checkForAccessToken',function(cb)
        {

            var dataToBeInserted = {
                User: userId,
                amount:payloadData.amount
            };
            Service.DonationServices.createDonation(dataToBeInserted, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })

};

module.exports = {
    getProfile: getProfile,
    signupWithEmail: signupWithEmail,
    createFBUser: createFBUser,
    fbLogin: fbLogin,
    emailLogin: emailLogin,
    accessTokenLogin: accessTokenLogin,
    logout: logout,
    reportViolation: reportViolation,
    listViolationPoints: listViolationPoints,
    listBPNIPosts: listBPNIPosts,
    listCounsellors: listCounsellors,
    listPetitions: listPetitions,
    signupPetition: signupPetition,
    getUserDetails: getUserDetails,
    CounsellorRequests: CounsellorRequests,
    editProfile: editProfile,
    listNotifications: listNotifications,
    changePushNotificationSettings: changePushNotificationSettings,
    listAboutBPNI: listAboutBPNI,
    updateRadius: updateRadius,
    forgotPassword:forgotPassword,
    donateMoney:donateMoney

};