'use strict';
/**
 * Created by prince on 12/7/15.
 */
var mongoose = require('mongoose');
var Config = require('../Config');
var SocketManager = require('../Lib/SocketManager');
var Service = require('../Services');
var async = require('async');

//Connect to MongoDB
mongoose.connect(Config.dbConfig.mongo.URI, function (err) {
    if (err) {
        console.log("DB Error: ", err);
        process.exit(1);
    } else {
        console.log('MongoDB Connected');
    }
});
exports.bootstrapAdmin = function (callback) {
    var adminData1 = {
        email: 'paras@bpni.com',
        password: 'e10adc3949ba59abbe56e057f20f883e'
    };
    var adminData2 = {
        email: 'prince@bpni.com',
        password: 'e10adc3949ba59abbe56e057f20f883e'
    };
    async.parallel([
        function (cb) {
            insertData(adminData1.email, adminData1, cb)
        },
        function (cb) {
            insertData(adminData2.email, adminData2, cb)
        }
    ], function (err, done) {
        callback(err, 'Bootstrapping finished');
    })
};


exports.bootstrapLanguage = function (callback) {
    var adminData = {
        name: 'ENGLISH',
        shortCode: 'EN',
        defaultLang: true,
    };
    async.parallel([
        function (cb) {
            insertLanguage(adminData, cb)
        }
    ], function (err, done) {
        callback(err, 'Bootstrapping finished');
    })


};




var insertLanguage = function(adminData,callback){

    var flag = true;
    async.auto({

        checkLang:function(cb){
            var query = {
                name:"ENGLISH"
            }

            var options = {lean:true};
            var proejections = {
                _id:1
            }
            
            Service.languageService.getLanguage(query,proejections,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        flag = false
                        cb(null)
                    }else{

                        cb(null)
                    }
                }
            })
        },
        insertLang:['checkLang',function(cb){
            

            if(flag == true){
                var obj = {
                    name:adminData.name,
                    shortCode:adminData.shortCode,
                    order:1
                }
                Service.languageService.createLanguage(obj,function(err,result){
                    if(err){
                        callback(err);
                    }else{
                        callback(null);
                    }
                })

            }else{
                cb(null)
            }
        }]

    },function(err,result){
        if(err){
            callback(err)
        }else{
            callback(null)
        }
    })


    
}









exports.bootstrapAppVersion = function (callback) {
    var appVersion1 = {
        latestIOSVersion: '100',
        latestAndroidVersion: '100',
        criticalAndroidVersion: '100',
        criticalIOSVersion: '100',
        appType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER
    };
    var appVersion2 = {
        latestIOSVersion: '100',
        latestAndroidVersion: '100',
        criticalAndroidVersion: '100',
        criticalIOSVersion: '100',
        appType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER
    };


    async.parallel([
        function (cb) {
            insertVersionData(appVersion1.appType, appVersion1, cb)
        },
        function (cb) {
            insertVersionData(appVersion2.appType, appVersion2, cb)
        }
    ], function (err, done) {
        callback(err, 'Bootstrapping finished For App Version');
    })


};

function insertVersionData(appType, versionData, callback) {
    var needToCreate = true;
    async.series([
        function (cb) {
        var criteria = {
            appType: appType
        };
        Service.AppVersionService.getAppVersion(criteria, {}, {}, function (err, data) {
            if (data && data.length > 0) {
                needToCreate = false;
            }
            cb()
        })
    }, function (cb) {
        if (needToCreate) {
            Service.AppVersionService.createAppVersion(versionData, function (err, data) {
                cb(err, data)
            })
        } else {
            cb();
        }
    }], function (err, data) {
        console.log('Bootstrapping finished for ' + appType);
        callback(err, 'Bootstrapping finished For Admin Data')
    })
}

function insertData(email, adminData, callback) {
    var needToCreate = true;
    async.series([function (cb) {
        var criteria = {
            email: email
        };
        Service.AdminServices.getAdmin(criteria, {}, {}, function (err, data) {
            if (data && data.length > 0) {
                needToCreate = false;
                console.log(data);
            }
            cb()
        })
    }, function (cb) {
        if (needToCreate) {
            Service.AdminServices.createAdmin(adminData, function (err, data) {
                console.log(err);
                cb(err, data)
            })
        } else {
            cb();
        }
    }], function (err, data) {
        console.log('Bootstrapping finished for ' + email);
        callback(err, 'Bootstrapping finished')
    })
}

//Start Socket Server
exports.connectSocket = SocketManager.connectSocket;


