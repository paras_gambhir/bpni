/**
 * Created by parasgambhir on 13/9/16.
 */



var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var NewEvents = new Schema({
    title:{type: String,trim: true, index: true,default:null, sparse: true},
    text: {type: String, trim: true, index: true, default: null, sparse: true},
    dateAdded:{type:Date,default:Date.now},
    isDeleted:{type:Boolean,default:false}
});


module.exports = mongoose.model('NewEvents', NewEvents);
