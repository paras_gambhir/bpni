/**
 * Created by parasgambhir on 13/9/16.
 */



var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var ViolationsReport = new Schema({
    title: {type: String, trim: true, index: true, default: null, sparse: true},
    subject: {type: String, trim: true, index: true, default: null, sparse: true},
    insertedDate: {type: Date,default:Date.now},
    textBox: {type: String, default:null},
    imageUrl: {type: String, default: null},
    imageUrl1: {type: String, default: null},
    imageUrl2: {type: String, default: null},
    imageUrl3: {type: String, default: null},
    User:{type: Schema.ObjectId, ref: 'Users', required: true}
});


module.exports = mongoose.model('ViolationsReport', ViolationsReport);
