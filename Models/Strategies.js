/**
 * Created by parasgambhir on 13/9/16.
 */



var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Strategies = new Schema({
    text: {type: String, trim: true, index: true, default: null, sparse: true},
    addedDate: {type: Date,default: Date.now}
});


module.exports = mongoose.model('Strategies', Strategies);
