/**
 * Created by parasgambhir on 13/9/16.
 */



var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Counsellor = new Schema({
    email: {type: String, trim: true, index: true, default: null, sparse: true},
    name: {type: String, trim: true, index: true, default: null, sparse: true},
    imageUrl:{type:String,trim:true},
    contactNo: {type: String, trim: true,default: null, sparse: true},
    latitude: {type: Number,default:0},
    longitude: {type: Number, default:0},
    address: {type: String, default: null},
    isDeleted: {type:Boolean,default:false}
});


module.exports = mongoose.model('Counsellor', Counsellor);
