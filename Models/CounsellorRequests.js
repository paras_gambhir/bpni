/**
 * Created by parasgambhir on 19/10/16.
 */

/**
 * Created by parasgambhir on 13/9/16.
 */



var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var CounsellorRequests = new Schema({
    User: {type: Schema.ObjectId, ref: 'Users',required:true},
    name: {type: String, trim: true,default: null, sparse: true},
    gender: {type: String, trim: true,default: null, sparse: true},
    age: {type: String, trim: true,default: null, sparse: true},
    imageUrl: {type: String,trim: true,default: null, sparse: true},
    description: {type: String,trim: true,default: null, sparse: true},
    email: {type: String, default: null},
    contactNo:{type:String,default:null},
    date:{type:Date,default:Date.now()}
});


module.exports = mongoose.model('CounsellorRequests', CounsellorRequests);
