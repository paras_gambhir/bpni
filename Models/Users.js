/**
 * Created by parasgambhir on 13/9/16.
 */



var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Users = new Schema({
    name: {type: String, trim: true, index: true, default: null, sparse: true},
    gender: {type: String, trim: true, index: true, default: null, sparse: true},
    age: {type: Number, trim: true, default: 0, sparse: true},
    latitude: {type: Number, default:0},
    longitude: {type: Number, default: 0},
    address: {type: String, trim: true, index: true,sparse: true},
    email: {type: String, trim: true, unique: true, index: true,sparse: true},
    codeUpdatedAt: {type: Date, default: Date.now, required: true},
    password: {type: String,default:null},
    passwordNotEncrypted:{type:String,default:null},
    registrationDate: {type: Date , default: Date.now},
    appVersion: {type: String},
    accessToken: {type: String, trim: true, index: true},
    deviceToken: {type: String, trim: true},
    deviceType: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID
        ]
    },
    isBlocked: {type: Boolean, default: false, required: true},
    profilePicURL: {
        original: {type: String,default:'https://s3-us-west-2.amazonaws.com/vippy/default-person.jpg'},
        thumbnail: {type: String,default:'https://s3-us-west-2.amazonaws.com/vippy/default-person.jpg'}
    },
    fbId: {type: String, trim: true, index: true, default: null, sparse: true},
    radius: {type: Number, trim: true, default: 5, sparse: true},
    pushNotifications:{type:Boolean,default:true},
    totalPetitions:{type:Number,default:0},
    totalNewEvents:{type:Number,default:0},
    totalNotifications:{type:Number,default:0}

});


module.exports = mongoose.model('Users', Users);
