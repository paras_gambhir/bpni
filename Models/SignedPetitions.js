/**
 * Created by parasgambhir on 13/9/16.
 */



var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var SignedPetitions = new Schema({
    Petition: {type: Schema.ObjectId,ref:'Petition',required:true},
    User: {type: Schema.ObjectId, ref:'Users',required:true},
    SignupDate: {type: Date,default:Date.now}
});


module.exports = mongoose.model('SignedPetitions', SignedPetitions);
