/**
 * Created by parasgambhir on 13/9/16.
 */



var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Petition = new Schema({
    imageUrl:{type:String,trim:true,default:null,sparse:true},
    name: {type: String, trim: true, index: true, default: null, sparse: true},
    date: {type: Date, default:Date.now},
    description: {type: String,default:null},
    isDeleted:{type:Boolean,default:false}
});


module.exports = mongoose.model('Petition', Petition);
