/**

 */
module.exports = {
    Users : require('./Users'),
    AppVersions : require('./AppVersions'),
    ViolationsReport: require('./ViolationsReport'),
    ViolationPoints:require('./ViolationPoints'),
    BPNIPosts:require('./BPNIPosts'),
    Counsellor: require('./Counsellor'),
    Donation: require('./Donation'),
    Petition:require('./Petition'),
    SignedPetitions:require('./SignedPetitions'),
    CounsellorRequests:require('./CounsellorRequests'),
    Strategies:require('./Strategies'),
    NewEvents:require('./NewEvents'),
    Admin:require('./Admin')
};