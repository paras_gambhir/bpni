/**
 * Created by parasgambhir on 13/9/16.
 */



var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var ViolationPoints = new Schema({
    points: {type: String, trim: true, index: true, default: null, sparse: true},
    isDeleted:{type:Boolean,default:false}
});


module.exports = mongoose.model('ViolationPoints', ViolationPoints);
