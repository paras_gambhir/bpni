/**
 * Created by parasgambhir on 13/9/16.
 */



var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Donation = new Schema({
    User: {type: Schema.ObjectId, ref: 'Users'},
    donatedOn: {type: Date,default: Date.now},
    amount:{type:Number,required:true}
});


module.exports = mongoose.model('Donation', Donation);
