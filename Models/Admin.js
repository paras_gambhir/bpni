/**
 * Created by parasgambhir on 13/9/16.
 */



var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Admin = new Schema({
    email: {type: String, trim: true, index: true, default: null, sparse: true,unique:true},
    password: {type: String, trim: true,default: null, sparse: true,index:true},
    accessToken: {type: String, trim: true,default: null, sparse: true,unique:true},
    tAndC: {type: String, default:null},
    aboutBPNI:{type:String,default:null},
    donationText:{type:String,default:"The Simplest act of kindness are by far more powerful than a thousand heads bowing in prayer"}
});


module.exports = mongoose.model('Admin', Admin);
