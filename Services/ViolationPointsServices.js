'use strict';

var Models = require('../Models');


//Insert User in DB
var createViolationPoints= function (objToSave, callback) {
    new Models.ViolationPoints(objToSave).save(callback)
};

//Update User in DB
var updateViolationPoints = function (criteria, dataToSet, options, callback) {
    Models.ViolationPoints.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteViolationPoints = function (criteria, callback) {
    Models.ViolationPoints.findOneAndRemove(criteria, callback);
};

var getViolationPoints = function (criteria, projection, options, callback) {
    Models.ViolationPoints.find(criteria, projection, options, callback);
};



module.exports = {
    createViolationPoints: createViolationPoints,
    updateViolationPoints:updateViolationPoints,
    deleteViolationPoints:deleteViolationPoints,
    getViolationPoints:getViolationPoints

};

