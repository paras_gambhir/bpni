'use strict';

var Models = require('../Models');


//Insert User in DB
var createNewEvents= function (objToSave, callback) {
    new Models.NewEvents(objToSave).save(callback)
};

//Update User in DB
var updateNewEvents = function (criteria, dataToSet, options, callback) {
    Models.NewEvents.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteNewEvents = function (criteria, callback) {
    Models.NewEvents.findOneAndRemove(criteria, callback);
};

var getNewEvents = function (criteria, projection, options, callback) {
    Models.NewEvents.find(criteria, projection, options, callback);
};



module.exports = {
    createNewEvents: createNewEvents,
    updateNewEvents:updateNewEvents,
    deleteNewEvents:deleteNewEvents,
    getNewEvents:getNewEvents

};

