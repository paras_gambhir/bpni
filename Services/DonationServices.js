'use strict';

var Models = require('../Models');


//Insert User in DB
var createDonation= function (objToSave, callback) {
    new Models.Donation(objToSave).save(callback)
};

//Update User in DB
var updateDonation = function (criteria, dataToSet, options, callback) {
    Models.Donation.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteDonation = function (criteria, callback) {
    Models.Donation.findOneAndRemove(criteria, callback);
};

var getDonation = function (criteria, projection, options, callback) {
    Models.Donation.find(criteria, projection, options, callback);
};

var getDonationPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Donation.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

module.exports = {
    createDonation: createDonation,
    updateDonation:updateDonation,
    deleteDonation:deleteDonation,
    getDonation:getDonation,
    getDonationPopulate:getDonationPopulate

};

