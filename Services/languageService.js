'use strict';

var Models = require('../Models');


//Insert User in DB
var createLanguage = function (objToSave, callback) {
    new Models.language(objToSave).save(callback)
};

//Update User in DB

var updateLanguage = function (criteria, dataToSet, options, callback) {
    Models.language.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB

var deleteLanguage = function (criteria, callback) {
    Models.language.findOneAndRemove(criteria, callback);
};

var getLanguage = function (criteria, projection, options, callback) {
    Models.language.find(criteria, projection, options, callback);
};

var getPopulateLanguage = function (criteria, projection, options,populate, callback) {
    Models.language.find(criteria, projection, options).populate(populate).exec(function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,result)
        }
    });
};


module.exports = {
    createLanguage : createLanguage,
    updateLanguage : updateLanguage,
    deleteLanguage :deleteLanguage ,
    getLanguage :getLanguage,
    getPopulateLanguage:getPopulateLanguage
};

