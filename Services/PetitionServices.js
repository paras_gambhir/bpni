'use strict';

var Models = require('../Models');


//Insert User in DB
var createPetition= function (objToSave, callback) {
    new Models.Petition(objToSave).save(callback)
};

//Update User in DB
var updatePetition = function (criteria, dataToSet, options, callback) {
    Models.Petition.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deletePetition = function (criteria, callback) {
    Models.Petition.findOneAndRemove(criteria, callback);
};

var getPetition = function (criteria, projection, options, callback) {
    Models.Petition.find(criteria, projection, options, callback);
};



module.exports = {
    createPetition: createPetition,
    updatePetition:updatePetition,
    deletePetition:deletePetition,
    getPetition:getPetition

};

