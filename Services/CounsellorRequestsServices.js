'use strict';

var Models = require('../Models');


//Insert User in DB
var createCounsellorRequests = function (objToSave, callback) {
    new Models.CounsellorRequests(objToSave).save(callback)
};

//Update User in DB
var updateCounsellorRequests = function (criteria, dataToSet, options, callback) {
    Models.CounsellorRequests.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteCounsellorRequests = function (criteria, callback) {
    Models.CounsellorRequests.findOneAndRemove(criteria, callback);
};

var getCounsellorRequests = function (criteria, projection, options, callback) {
    Models.CounsellorRequests.find(criteria, projection, options, callback);
};

var getCounsellorRequestsPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.CounsellorRequests.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};


module.exports = {
    createCounsellorRequests: createCounsellorRequests,
    updateCounsellorRequests:updateCounsellorRequests,
    deleteCounsellorRequests:deleteCounsellorRequests,
    getCounsellorRequests:getCounsellorRequests,
    getCounsellorRequestsPopulate:getCounsellorRequestsPopulate

};

