/**
 * Created by prince
 */
module.exports = {
    UserServices:  require('./UserServices'),
    AdminServices: require('./AdminServices'),
    AppVersionServices: require('./AppVersionService'),
    BPNIPostsServices: require('./BPNIPostsServices'),
    CounsellorServices: require('./CounsellorServices'),
    CounsellorRequestsServices: require('./CounsellorRequestsServices'),
    DonationServices: require('./DonationServices'),
    NewEventsServices: require('./NewEventsServices'),
    PetitionServices: require('./PetitionServices'),
    SignedPetitionServices: require('./SignedPetitionServices'),
    StrategiesServices: require('./StrategiesServices'),
    ViolationPointsServices: require('./ViolationPointsServices'),
    ViolationReportServices: require('./ViolationReportServices')

};