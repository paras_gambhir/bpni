'use strict';

var Models = require('../Models');


//Insert User in DB
var createAdmin= function (objToSave, callback) {
    new Models.Admin(objToSave).save(callback)
};

//Update User in DB
var updateAdmin = function (criteria, dataToSet, options, callback) {
    Models.Admin.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteAdmin = function (criteria, callback) {
    Models.Admin.findOneAndRemove(criteria, callback);
};

var getAdmin = function (criteria, projection, options, callback) {
    Models.Admin.find(criteria, projection, options, callback);
};

var updateMulti = function (criteria, dataToSet, options, callback) {
    Models.Admin.update(criteria, dataToSet, options, callback);
};

module.exports = {
    createAdmin: createAdmin,
    updateAdmin:updateAdmin,
    deleteAdmin:deleteAdmin,
    getAdmin:getAdmin,
    updateMulti:updateMulti

};

