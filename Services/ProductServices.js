'use strict';

var Models = require('../Models');


//Insert User in DB
var createProducts= function (objToSave, callback) {
    new Models.Products(objToSave).save(callback)
};

//Update User in DB
var updateProducts = function (criteria, dataToSet, options, callback) {
    Models.Products.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteProducts = function (criteria, callback) {
    Models.Products.findOneAndRemove(criteria, callback);
};

var getProducts = function (criteria, projection, options, callback) {
    Models.Products.find(criteria, projection, options, callback);
};



module.exports = {
    createProducts: createProducts,
    updateProducts:updateProducts,
    deleteProducts:deleteProducts,
    getProducts:getProducts

};

