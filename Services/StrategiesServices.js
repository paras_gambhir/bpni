'use strict';

var Models = require('../Models');


//Insert User in DB
var createStrategies= function (objToSave, callback) {
    new Models.Strategies(objToSave).save(callback)
};

//Update User in DB
var updateStrategies = function (criteria, dataToSet, options, callback) {
    Models.Strategies.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteStrategies = function (criteria, callback) {
    Models.Strategies.findOneAndRemove(criteria, callback);
};

var getStrategies = function (criteria, projection, options, callback) {
    Models.Strategies.find(criteria, projection, options, callback);
};



module.exports = {
    createStrategies: createStrategies,
    updateStrategies:updateStrategies,
    deleteStrategies:deleteStrategies,
    getStrategies:getStrategies

};

