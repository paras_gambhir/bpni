'use strict';

var Models = require('../Models');


//Insert User in DB
var createFso= function (objToSave, callback) {
    new Models.FsoDetails(objToSave).save(callback)
};

//Update User in DB
var updateFso = function (criteria, dataToSet, options, callback) {
    Models.FsoDetails.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteFso = function (criteria, callback) {
    Models.FsoDetails.findOneAndRemove(criteria, callback);
};

var getFso = function (criteria, projection, options, callback) {
    Models.FsoDetails.find(criteria, projection, options, callback);
};



module.exports = {
    createFso: createFso,
    updateFso:updateFso,
    deleteFso:deleteFso,
    getFso:getFso

};

