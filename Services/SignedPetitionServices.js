'use strict';

var Models = require('../Models');


//Insert User in DB
var createSignedPetition= function (objToSave, callback) {
    new Models.SignedPetitions(objToSave).save(callback)
};

//Update User in DB
var updateSignedPetition = function (criteria, dataToSet, options, callback) {
    Models.SignedPetitions.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteSignedPetition = function (criteria, callback) {
    Models.SignedPetitions.findOneAndRemove(criteria, callback);
};

var getSignedPetition = function (criteria, projection, options, callback) {
    Models.SignedPetitions.find(criteria, projection, options, callback);
};

var getSignedPetitionsPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.SignedPetitions.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};


module.exports = {
    createSignedPetition: createSignedPetition,
    updateSignedPetition:updateSignedPetition,
    deleteSignedPetition:deleteSignedPetition,
    getSignedPetition:getSignedPetition,
    getSignedPetitionsPopulate:getSignedPetitionsPopulate

};

