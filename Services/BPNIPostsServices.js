'use strict';

var Models = require('../Models');


//Insert User in DB
var createBPNIPosts= function (objToSave, callback) {
    new Models.BPNIPosts(objToSave).save(callback)
};

//Update User in DB
var updateBPNIPosts = function (criteria, dataToSet, options, callback) {
    Models.BPNIPosts.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteBPNIPosts = function (criteria, callback) {
    Models.BPNIPosts.findOneAndRemove(criteria, callback);
};

var getBPNIPosts = function (criteria, projection, options, callback) {
    Models.BPNIPosts.find(criteria, projection, options, callback);
};



module.exports = {
    createBPNIPosts: createBPNIPosts,
    updateBPNIPosts:updateBPNIPosts,
    deleteBPNIPosts:deleteBPNIPosts,
    getBPNIPosts:getBPNIPosts

};

