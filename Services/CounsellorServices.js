'use strict';

var Models = require('../Models');


//Insert User in DB
var createCounsellor= function (objToSave, callback) {
    new Models.Counsellor(objToSave).save(callback)
};

//Update User in DB
var updateCounsellor = function (criteria, dataToSet, options, callback) {
    Models.Counsellor.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteCounsellor = function (criteria, callback) {
    Models.Counsellor.findOneAndRemove(criteria, callback);
};

var getCounsellor = function (criteria, projection, options, callback) {
    Models.Counsellor.find(criteria, projection, options, callback);
};



module.exports = {
    createCounsellor: createCounsellor,
    updateCounsellor:updateCounsellor,
    deleteCounsellor:deleteCounsellor,
    getCounsellor:getCounsellor

};

