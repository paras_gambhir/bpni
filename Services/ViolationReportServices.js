'use strict';

var Models = require('../Models');


//Insert User in DB
var createViolationReports= function (objToSave, callback) {
    new Models.ViolationsReport(objToSave).save(callback)
};

//Update User in DB
var updateViolationReports = function (criteria, dataToSet, options, callback) {
    Models.ViolationsReport.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteViolationReports = function (criteria, callback) {
    Models.ViolationsReport.findOneAndRemove(criteria, callback);
};

var getViolationReports = function (criteria, projection, options, callback) {
    Models.ViolationsReport.find(criteria, projection, options, callback);
};

var getViolationReportsPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.ViolationsReport.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};


module.exports = {
    createViolationReports: createViolationReports,
    updateViolationReports:updateViolationReports,
    deleteViolationReports:deleteViolationReports,
    getViolationReports:getViolationReports,
    getViolationReportsPopulate:getViolationReportsPopulate

};

